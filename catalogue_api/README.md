# INSTALLATION

* Créer dossier "data" dans catalogue_api.
* Ajouter les fichiers "catégories.json" et "sandwichs.json" dans le dossier "data".
* Exécuter `docker-compose up` à la racine du projet.
* Exécuter `docker-compose exec mongo.cat mongoimport --db mongo --collection categories --file /var/data/categories.json --jsonArray` à la racine du projet.
* Exécuter `docker-compose exec mongo.cat mongoimport --db mongo --collection sandwichs --file /var/data/sandwichs.json --jsonArray` à la racine du projet.