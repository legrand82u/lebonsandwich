define({ "api": [
  {
    "type": "get",
    "url": "/",
    "title": "route d'accueil de l'api commande",
    "description": "<p>affiche un message de bienvenue</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messageBienvenue",
            "description": "<p>message de bienvenue</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "Get"
  },
  {
    "type": "get",
    "url": "/categories",
    "title": "récupération de toutes les catégories",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec toutes les catégories</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "GetCategories"
  },
  {
    "type": "get",
    "url": "/categories/:id",
    "title": "récupération des infos d'une catégorie",
    "description": "<p>récupération des infos d'une catégorie</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de la catégorie concernée</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>json avec les infos de la catégorie</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "GetCategoriesId"
  },
  {
    "type": "get",
    "url": "/categories/:id/sandwichs",
    "title": "affiche les sandwichs d'une catégorie",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de la catégorie concernée</p>"
          }
        ]
      }
    },
    "description": "<p>affiche les sandwichs d'une catégorie</p>",
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "GetCategoriesIdSandwichs"
  },
  {
    "type": "get",
    "url": "/sandwichs/:id",
    "title": "affiche les informations d'un sandwich",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id du sandwich concerné</p>"
          }
        ]
      }
    },
    "description": "<p>affiche les informations d'un sandwich</p>",
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "GetSandwichsId"
  },
  {
    "type": "post",
    "url": "/categories",
    "title": "ajoute une nouvelle catégorie",
    "description": "<p>ajoute une nouvelle catégorie</p>",
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/catalogue_dev/server.js",
    "name": "PostCategories"
  }
] });
