const mongoose = require("mongoose");

const schema = new mongoose.Schema({
  ref: { type: String, unique: true, required: true },
  nom: { type: String, required: true },
  description: { type: String, text: true },
  type_pain: { type: String, text: true },
  image: { type: String },
  categories: { type: String, text: true },
  prix: { type: Number }
});

module.exports = mongoose.model("Sandwich", schema, "sandwichs");
