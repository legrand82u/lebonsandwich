"use strict";

const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const uuidv1 = require('uuid/v1');

//import des modeles
const Category = require("./models/Category");
const Sandwich = require("./models/Sandwich");

const PORT = 8080;
const HOST = "0.0.0.0";

const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false
  })
);

const db_name = "mongo.cat:dbcat/mongo";

//connexion à la bdd mongo
mongoose.connect("mongodb://" + db_name, {
  useNewUrlParser: true,
  useUnifiedTopology: true
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                Routes                                                              //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @api {get} / route d'accueil de l'api commande
 * @apiDescription affiche un message de bienvenue
 * @apiSuccess {string} messageBienvenue message de bienvenue
 */
app.get("/", (req, res) => {
  res.send("Bienvenue sur catalogue API\n");
});

/**
 * @api {get} /categories récupération de toutes les catégories
 * @apiSuccess {json} json avec toutes les catégories
 */
app.get("/categories", (req, res) => {
  Category.find({}, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }

    res.status(200).json(result);
  });
});

/**
 * @api {get} /categories/:id récupération des infos d'une catégorie
 * @apiDescription récupération des infos d'une catégorie
 * @apiParam {Number} id id de la catégorie concernée
 * @apiSuccess {json} json json avec les infos de la catégorie
 */
app.get("/categories/:id", (req,res) => {
  let id = req.params.id;
  Category.find({"id": id},'-_id',(err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    let date = new Date();
    let resultat = {
      "type": "resource",
      "date": date.getDate()+"-"+(parseInt(date.getMonth())+1)+"-"+date.getFullYear(),
      "categorie": {
        "id": result[0].id,
        "nom": result[0].nom,
        "description": result[0].description
      },
      "links": {
        "sandwichs": {"href": "/categories/" + id + "/sandwichs/"},
        "self": {"href": "/categories/" + id + "/"}
      }
    };
    res.status(200).json(resultat);
  });
});

/**
 * @api {post} /categories ajoute une nouvelle catégorie
 * @apiDescription ajoute une nouvelle catégorie
 */
app.post("/categories", (req, res) => {
  res.set("Accept", "application/json");
  res.type("application/json");

  if (!req.is("application/json") || req.body.length == 0) {
    res.status(406).send("Not Acceptable");
  } else {

    //la méthode get_next_id est asynchrone. Elle retourne un résultat sous forme de Promesse. 
    get_next_id().then(result => {

      //si la valeur de id peut être de type alphanumérique et unique, on peut utiliser le module uuid
      //req.body.id = uuidv1();

      //sinon on créé dynamiquement un nouvel id de type numérique auto-incrémenté comme ce serait le cas dans une bdd MySQL
      req.body.id = result;

      Category.create(req.body, function (err, result) {
        if (err) {
          res.status(500).send(err.errmsg);
        } else {
          res.header(
            "Location",
            "/categories/" + result.id
          );
          res.set("Accept", "application/json");
          res.type("application/json");
          res.status(201).send(result);
        }
      });
    }).catch(err => {
      throw new Error(err);
    }
    );

  }
});

/**
 * @api {get} /sandwichs/:id affiche les informations d'un sandwich
 * @apiParam {Number} id id du sandwich concerné
 * @apiDescription affiche les informations d'un sandwich
 */
app.get("/sandwichs/:id",(req,res) => {


  Sandwich.find({ref: req.params.id}, "-_id", (err,result) => {
    if (err) {
      res.status(500).send(err);
    }
    result = result[0];
    let json = {
      "nom": result.nom,
      "description": result.description,
      "prix": result.prix,
      "uri": "/sandwichs/" + result.ref
    };
    res.status(200).json(json);
  })
});

/**
 * @api {get} /categories/:id/sandwichs affiche les sandwichs d'une catégorie
 * @apiParam {Number} id id de la catégorie concernée
 * @apiDescription affiche les sandwichs d'une catégorie
 */
app.get("/categories/:id/sandwichs", (req, res) => {
  Category.find({ id: req.params.id }, (err, result) => {
    if (err) {
      res.status(500).send(err);
    }
    Sandwich.find({categories: new RegExp("^[a-zA-Z,]*" + result[0].nom + "[a-zA-Z,]*")} , (erreur, resultat) => {
      if (erreur) {
        res.status(500).send(erreur);
      }

      res.header("Content-Type", "application/json; charset=utf-8");
      res.status(200);
      res.end(JSON.stringify({resultat, count: resultat.length }));
    });
  });
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                      Fonctions                                                                     //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//get_next_id fourni la valeur du prochain id numérique disponible
//ne pas confondre l'attribut _id de type alphanumérique automatiquement renseigné par MongoDB
//et l'attribut id de type numérique entier dont la valeur est auto-incrémentale, généré par la méthode get_next_id
function get_next_id() {
  return new Promise((resolve, reject) => {
    Category.findOne().sort('-id').limit(1).exec((err, result) => {
      if (err) reject(err);
      (result) ? resolve(result.id + 1) : resolve(1);
    });
  });
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
app.listen(PORT, HOST);
console.log(`Catalogue API Running on http://${HOST}:${PORT}`);
