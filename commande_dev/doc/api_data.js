define({ "api": [
  {
    "type": "all",
    "url": "/commandes/:id",
    "title": "interdit toute intéraction avec une commande, execepté les routes autorisées",
    "description": "<p>interdit toute intéraction avec une commande, execepté les routes autorisées</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id de la commande en question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "AllCommandesId"
  },
  {
    "type": "all",
    "url": "/items/:id",
    "title": "interdit l'intéraction avec un item autre qu'avec les autres verbes HTTP définies avant",
    "description": "<p>interdit l'intéraction avec un item autre qu'avec les autres verbes HTTP définies avant</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de l'item en question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "AllItemsId"
  },
  {
    "type": "get",
    "url": "/",
    "title": "route d'accueil de l'api commande",
    "description": "<p>affiche un message de bienvenue</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messageBienvenue",
            "description": "<p>message de bienvenue</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "Get"
  },
  {
    "type": "get",
    "url": "/clients",
    "title": "retourne les clients disponibles en bdd",
    "description": "<p>retourne les clients disponibles en bdd</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les clients</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetClients"
  },
  {
    "type": "get",
    "url": "/clients/:id",
    "title": "retourne les informations sur un client",
    "description": "<p>retourne les informations sur un client</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les infos sur le client</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id du client en question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetClientsId"
  },
  {
    "type": "get",
    "url": "/commandes",
    "title": "retourne les commandes disponibles en bdd",
    "description": "<p>retourne les commandes disponibles en bdd</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les commandes</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetCommandes"
  },
  {
    "type": "get",
    "url": "/commandes/:id",
    "title": "retourne les informations d'une commande",
    "description": "<p>retourne les informations d'une commande</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les infos d'une commande</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id de la commande en question</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "token",
            "description": "<p>token de la commande, possibilité de l'entrer en header de la requête</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token de la commande, si non entré dans l'url</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetCommandesId"
  },
  {
    "type": "get",
    "url": "/commandes/:id/items",
    "title": "retourne les items d'une commande",
    "description": "<p>retourne les items d'une commande</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les items d'une commande</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id de la commande en question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetCommandesIdItems"
  },
  {
    "type": "get",
    "url": "/items",
    "title": "retourne tous les items",
    "description": "<p>retourne tous les items disponibles</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les items disponibles en bdd</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetItems"
  },
  {
    "type": "get",
    "url": "/items/:id",
    "title": "retourne toutes les infos d'un item",
    "description": "<p>retourne toutes les infos d'un item</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les items disponibles en bdd</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de l'item en question</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "GetItemsId"
  },
  {
    "type": "post",
    "url": "/clients/:id/auth",
    "title": "permet l'authentification",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": false,
            "field": "id",
            "description": "<p>id de l'utilisateur</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>&quot;Basic chaineEncodeeB64&quot; avec chaineEncodeeB64 qui vaut &quot;email:mdp&quot; le tout encodé en base 64</p>"
          }
        ]
      }
    },
    "description": "<p>permet l'authentification</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "JSON",
            "optional": false,
            "field": "tokenJWT",
            "description": "<p>json avec le token d'authentification jwt et le code de retour</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "PostClientsIdAuth"
  },
  {
    "type": "post",
    "url": "/commandes",
    "title": "ajotue une commande",
    "description": "<p>ajoute une commande</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "authorization",
            "description": "<p>token d'authentification JWT</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "PostCommandes"
  },
  {
    "type": "put",
    "url": "/commandes/:id",
    "title": "modifie les informations d'une commande",
    "description": "<p>modifie les informations d'une commande</p>",
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité</p>"
          }
        ]
      }
    },
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>id de la commande en question</p>"
          },
          {
            "group": "Parameter",
            "type": "json",
            "optional": false,
            "field": "req.body",
            "description": "<p>json contenant les informations de la commande</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": true,
            "field": "token",
            "description": "<p>token de la commande, possibilité de l'entrer en header de la requête</p>"
          }
        ]
      }
    },
    "header": {
      "fields": {
        "Header": [
          {
            "group": "Header",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>token de la commande, si non entré dans l'url</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/commande_dev/server.js",
    "name": "PutCommandesId"
  }
] });
