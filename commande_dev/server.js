"use strict";

const express = require("express");
const mysql = require("mysql");
const fs = require("file-system");
const jwt = require('jsonwebtoken');
// Constantes
const PORT = 8080;
const HOST = "0.0.0.0";

// Gestion du token
const bcrypt = require('bcrypt');
const saltRounds = 10;
const bcrypt_password = 'jesuislemotdepasse';

const axios = require("axios");
// App
const bodyParser = require('body-parser');


// CORS
const cors = require('cors');
// On peut remplacer cette URL par les sites que l'on voudra autoriser par la suite, pour le moment, étant donné qu'on n'a pas d'application client on ne peut pas vraiment utiliser cette fonctionnalité.
var whitelist = ['http://example1.com'];
// On autorise les requêtes ne présentant pas d'origine afin d'autoriser Postman à effectuer les requêtes.
const corsOptions = {
    origin: function (origin, callback) {
        if (whitelist.indexOf(origin) !== -1 || !origin) {
            callback(null, true)
        } else {
            callback(new Error('Not allowed by CORS'))
        }
    }
};
const uuid = require('uuid/v1');

const validator = require('validator');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors(corsOptions));

/**
 * @api {get} / route d'accueil de l'api commande
 * @apiDescription affiche un message de bienvenue
 * @apiSuccess {string} messageBienvenue message de bienvenue
 */
app.get("/", (req, res) => {
    res.status(200).end("Bienvenue sur Commande API");
});

/**
 * @api {get} /items retourne tous les items
 * @apiDescription retourne tous les items disponibles
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les items disponibles en bdd
 */
app.get("/items", (req, res) => {
    let queryItems = "SELECT * FROM item";
    db.query(queryItems, [], (err, result) => {
        if (err) {
            res.status(500);
            res.end(JSON.stringify(getMessageFromHTTPCode(500)));
        }
        res.send(result);
    })
});

/**
 * @api {get} /items/:id retourne toutes les infos d'un item
 * @apiDescription retourne toutes les infos d'un item
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les items disponibles en bdd
 * @apiParam {Number} id id de l'item en question
 */
app.get("/items/:id", (req, res) => {
    let queryItem = "SELECT * FROM item where id=?";
    db.query(queryItem, [req.params.id], (err, result) => {
            if (err) {
                res.status(500);
                res.end(getMessageFromHTTPCode(500));
            }
            if (result.length <= 0) {
                res.header("Content-Type", "application/json");
                res.status(404).end(getMessageFromHTTPCode(404));
            }
            res.send(result)
        }
    )
});

/**
 * @api {all} /items/:id interdit l'intéraction avec un item autre qu'avec les autres verbes HTTP définies avant
 * @apiDescription interdit l'intéraction avec un item autre qu'avec les autres verbes HTTP définies avant
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiParam {Number} id id de l'item en question
 */
app.all("/items/:id", (req, res) => {
    res.status(405).end(getMessageFromHTTPCode(405));
});

/**
 * @api {get} /commandes retourne les commandes disponibles en bdd
 * @apiDescription retourne les commandes disponibles en bdd
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les commandes
 */
app.get("/commandes", (req, res) => {
    let queryCommande = "SELECT * FROM commande";
    db.query(queryCommande, [],(err, result) => {
        if (err) {
            res.status(500);
            res.end(getMessageFromHTTPCode(500));
        }
        res.send(result);
    })
});

/**
 * @api {get} /commandes/:id/items retourne les items d'une commande
 * @apiDescription retourne les items d'une commande
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les items d'une commande
 * @apiParam {String} id id de la commande en question
 */
app.get("/commandes/:id/items", (req, res) => {
    let promise = getItemsFromCommande(req.params.id);
    promise.then((result) => {
        let resultat = {
            "items": result
        };
        res.send(resultat);
    }).catch(error => {
        res.status(error);
        res.end(getMessageFromHTTPCode(error));
    });
});

/**
 * @api {get} /commandes/:id retourne les informations d'une commande
 * @apiDescription retourne les informations d'une commande
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les infos d'une commande
 * @apiParam {String} id id de la commande en question
 * @apiHeader {String} token token de la commande, si non entré dans l'url
 * @apiParam {String} [token] token de la commande, possibilité de l'entrer en header de la requête
 */
app.get("/commandes/:id", (req, res) => {

    //vérification du token
    let token = req.query.token;
    if (typeof token === "undefined") {
        token = req.headers["x-lbs-token"];
    }
    if (typeof token === "undefined") {
        res.status(403);
        res.header("Content-Type", "application/json");
        res.end(JSON.stringify(getMessageFromHTTPCode(403)));
    }

    if (!bcrypt.compareSync(bcrypt_password, token)) {
        res.status(403);
        res.header("Content-Type", "application/json");
        res.end(JSON.stringify(getMessageFromHTTPCode(403)));
    }

    let queryItem = "SELECT * FROM commande where id=?";
    db.query(queryItem, [req.params.id], (err, result) => {
            if (err) {
                res.status(500);
                res.header("Content-Type", "application/json");
                res.end(getMessageFromHTTPCode(500));
            }
            if (result.length <= 0 || typeof result == "undefined") {
                res.header("Content-Type", "application/json; charset=utf-8");
                res.status(404).end(getMessageFromHTTPCode(404));
            }
            if (result[0].token !== token) {
                res.header("Content-Type", "application/json; charset=utf-8");
                res.status(404).end(getMessageFromHTTPCode(404));
            }
            result = result[0];
            let resultat = {};
            resultat.type = "resource";
            resultat.links = {
                "self": "/commandes/" + req.params.id + "/",
                "items": "/commandes/" + req.params.id + "/items"
            };
            let promiseItems = getItemsFromCommande(req.params.id);
            promiseItems.then((resultatItems) => {
                resultat.command = {
                    "id": result.id,
                    "created_at": result.created_at,
                    "livraison": result.livraison,
                    "nom": result.nom,
                    "mail": result.mail,
                    "montant": result.montant,
                    "items": resultatItems
                };
                res.send(resultat);
            })
        }
    )
});

/**
 * @api {post} /commandes ajotue une commande
 * @apiDescription ajoute une commande
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiHeader {String} authorization token d'authentification JWT
 */
app.post("/commandes", async (req, res) => {
    //vérifications commande fidelisée
    let estCommandeFidelisee = false;
    let user_id = req.headers.id;
    if (typeof user_id !== "undefined") {
        let jwt_token = req.headers.authorization;
        if (typeof jwt_token === "undefined") {
            res.status(401).header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(401));
        }
        //vérifications de la véracité du token
        let codeValidToken;
        try {
            codeValidToken = await isValidTokenForSelectUser(user_id, jwt_token);
        } catch (e) {
            codeValidToken = e;
        }
        if (codeValidToken === 200) {
            estCommandeFidelisee = true;
        } else {
            res.status(codeValidToken).header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(codeValidToken));
        }
    }

    // Espace token
    let salt = bcrypt.genSaltSync(saltRounds);
    let hash = bcrypt.hashSync(bcrypt_password, salt);


    let id = uuid();
    let mandatoryData = isMandatoryDataPresent(req.body);
    let formatVerifie = verifFormat(req.body);
    if (mandatoryData[0] && formatVerifie[0]) {
        // Test si email fournie dans post identique a email en BDD
        try {
            await checkEmails(req.body.commande.mail, user_id);
        } catch (err) {
           return res.status(err).header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(err));
        }

        getPrix(req, id).then(montant => {
            req.body.commande.montant = montant;
            let query;
            if (estCommandeFidelisee) {
                query = createInsertCommandeQuery(id, hash, req.body.commande, user_id);
            } else {
                query = createInsertCommandeQuery(id, hash, req.body.commande);
            }
            db.query(query, (err, result) => {
                if (err) {
                    res.status(500);
                    res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
                }
                query = "SELECT * FROM commande WHERE id LIKE ?";
                db.query(query, [id], (err, result) => {
                    if (err) {
                        res.status(500);
                        res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
                    }

                    if (result.length <= 0) {
                        res.status(404);
                        res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(404));
                    }

                    let resultat = {};
                    resultat.type = "resource";
                    resultat.links = {
                        "self": "/commandes/" + id + "/",
                        "items": "/commandes/" + id + "/items"
                    };
                    result = result[0];
                    resultat.command = {
                        "nom": result.nom,
                        "mail": result.mail,
                        "livraison": {
                            "date": req.body.commande.livraison.date,
                            "heure": req.body.commande.livraison.heure
                        },
                        "id": result.id,
                        "token": hash,
                        "montant": montant,
                        "items": req.body.commande.items
                    };

                    //ajout du cumul total si commande fidélisée
                    if (estCommandeFidelisee) {
                        query = "UPDATE client set cumul_achats = (SELECT cumul_achats FROM client where id=?)+montant where id=?";
                        db.query(query, [user_id, user_id],(err, res) => {
                            if (err) {
                                res.status(500);
                                res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
                            }
                        })
                    }


                    res.status(201).header("Location", req.protocol + '://' + req.get('host') + "/commandes/" + id);
                    res.header("Content-Type", "application/json; charset=utf-8");
                    res.send(JSON.stringify(resultat));
                });
            });

        }).catch(err => {
            throw new Error(err)
        });
    } else {
        res.status(400);
        if (!mandatoryData[0]) {
            res.end(mandatoryData[1]);
        } else {
            res.end(formatVerifie[1]);
        }
    }
});


/**
 * @api {put} /commandes/:id modifie les informations d'une commande
 * @apiDescription modifie les informations d'une commande
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiParam {String} id id de la commande en question
 * @apiParam {json} req.body json contenant les informations de la commande
 * @apiHeader {String} token token de la commande, si non entré dans l'url
 * @apiParam {String} [token] token de la commande, possibilité de l'entrer en header de la requête
 */
app.put("/commandes/:id", (req, res) => {

    let token = req.query.token;
    if (typeof token === "undefined") {
        token = req.headers["x-lbs-token"];
    }
    if (typeof token === "undefined") {
        res.status(403);
        res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(403));
    }

    if (!bcrypt.compareSync(bcrypt_password, token)) {
        res.status(403);
        res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(403));
    }

    let id = req.params.id;
    let commandeSQL = "SELECT * FROM commande WHERE id=?";

    db.query(commandeSQL, [id], (error, result) => {
        if (error) {
            res.status(500);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
        }
        if (result.length <= 0) {
            res.header("Content-Type", "application/json; charset=utf-8");
            res.status(404).send(getMessageFromHTTPCode(404))
        }
        if (result[0].token !== token) {
            res.status(403).end(JSON.stringify({
                error: "403 unauthorized, token is wrong",
                code: 403,
                entryToken: token
            }))
        }
    });

    if (req.body.nom == null || req.body.mail == null || req.body.date == null) {
        let error = {
            type: "error",
            error: 400,
            message: "Paramètre manquant dans la requête"
        };
        res.header("Content-Type", "application/json; charset=utf-8");
        res.status(400).send(JSON.stringify(error));
    }
    let updateQuery = "UPDATE commande set nom=" + db.escape(req.body.nom) + ", mail=" + db.escape(
        req.body.mail) + ", livraison=DATE(" + db.escape(req.body.date) + ") WHERE id='" + id + "';";


    db.query(updateQuery, (erreur, resultat) => {
        if (erreur) {
            res.status(500);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
        }
        res.status(200);
        res.header("Content-Type", "application/json; charset=utf-8").end(JSON.stringify({value: "sucess", code: 200}));
    });

});

/**
 * @root /commandes/:id
 * @method all
 * @param id, id de la commande concernée
 * retourne une erreur car la méthode n'est pas bonne
 */
/**
 * @api {all} /commandes/:id interdit toute intéraction avec une commande, execepté les routes autorisées
 * @apiDescription interdit toute intéraction avec une commande, execepté les routes autorisées
 * @apiParam {String} id id de la commande en question
 */
app.all("/commandes/:id", (req, res) => {
    res.status(405).end(getMessageFromHTTPCode(405));
});

/**
 * @api {get} /clients retourne les clients disponibles en bdd
 * @apiDescription retourne les clients disponibles en bdd
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les clients
 */
app.get("/clients", (req, res) => {
    let queryClient = "SELECT * FROM client";
    db.query(queryClient, (err, result) => {
        if (err) {
            res.status(500);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
        }
        res.header("Content-Type", "application/json; charset=utf-8").send(result);
    })
});

/**
 * @api {get} /clients/:id retourne les informations sur un client
 * @apiDescription retourne les informations sur un client
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 * @apiSuccess {json} json avec les infos sur le client
 * @apiParam {Number} id id du client en question
 */
app.get("/clients/:id", async (req, res) => {
    let codeValidToken;
    try {
        codeValidToken = await isValidTokenForSelectUser(req.params.id, req.headers.authorization);
    } catch (e) {
        codeValidToken = e;
    }
    if (codeValidToken === 200) {
        let queryItem = "SELECT * FROM client where id=?";
        db.query(queryItem, [req.params.id],(err, result) => {
                if (err) {
                    res.status(500);
                    res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
                }
                if (result.length <= 0) {
                    res.header("Content-Type", "application/json; charset=utf-8");
                    res.status(404).end(getMessageFromHTTPCode(404));
                }
                res.send(result)
            }
        )
    } else {
        res.status(codeValidToken).header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(codeValidToken));
    }
});

/**
 * @api {post} /clients/:id/auth permet l'authentification
 * @apiParam {Number} id id de l'utilisateur
 * @apiHeader {String} authorization "Basic chaineEncodeeB64" avec chaineEncodeeB64 qui vaut "email:mdp" le tout encodé en base 64
 * @apiDescription permet l'authentification
 * @apiSuccess {JSON} tokenJWT json avec le token d'authentification jwt et le code de retour
 * @apiError {json} json avec le code d'erreur et un message explicatif volontairement générique pour des raisons de sécurité
 */
app.post("/clients/:id/auth", (req, res) => {
    let errorMessage = {
        type: "error",
        error: 401,
        message: "no autorization header present",
        headers: req.headers
    };

    let autorization = req.headers["authorization"];

    if (typeof autorization === "undefined") {
        res.status(401);
        res.end(JSON.stringify(errorMessage));
    }

    let dataDecodedB64 = autorization.split(" ");
    let buffer = new Buffer(dataDecodedB64[1], 'base64');
    dataDecodedB64 = buffer.toString('ascii');

    dataDecodedB64 = dataDecodedB64.split(':');
    let emailGiven = dataDecodedB64['0'];
    emailGiven = emailGiven.toLowerCase().replace(/ /g, "");
    let passwordGiven = dataDecodedB64['1'];

    let id = req.params.id;

    db.query("select mail_client, passwd, nom_client from client where id=?", [id], (error, result) => {
        if (error) {
            res.status(500);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500))
        }
        if (result.length <= 0) {
            res.status(404);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(404))
        }

        let verifm2p = bcrypt.compareSync(passwordGiven, result[0].passwd);
        let verifLogin = (result[0].mail_client.toLowerCase().replace(/ /g, "") === emailGiven);
        
        if (!verifLogin || !verifm2p){
            res.status(403).header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(403))
        }

        //génération du token

        const privateKey = fs.readFileSync('private.key');
        const token = jwt.sign(
            {sub: result[0].mail_client},
            privateKey,
            {expiresIn: '1h'}
        );

        res.status(200).header("Content-Type", "application/json; charset=utf-8").end(JSON.stringify({tokenJWT: token, code: 200}));
    })

});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                  Fin des routes                                                                    //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @root *
 * @method all
 * envoie une erreur : bad request
 */
app.all("*", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(400);
    res.end(JSON.stringify({type: "error", error: 400, message: "Requête daubée du cul : " + req.url}));
});

// lance l'application
app.listen(PORT, HOST);
console.log(`Commande API Running on http://${HOST}:${PORT}`);

//créé la bdd
const db = mysql.createConnection({
    host: "mysql.commande",
    user: "command_lbs",
    password: "command_lbs",
    database: "command_lbs"
});

// connexion à la bdd
db.connect(err => {
    if (err) {
        throw err;
    }
    console.log("Connected to database");
});

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                          Fonctions                                                                 //
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Generation de la requete SQL pour ajouter une commande
 * @param id identifiant de la commande
 * @param hash token de la commande
 * @param commande Object commande
 * @returns {string}
 */
function createInsertCommandeQuery(id, hash, commande, client_id = null) {
    let date = createDateObject(commande.livraison);
    if (client_id !== null) {
        return "INSERT INTO commande (id,nom,montant,mail,livraison,created_at, token, client_id) VALUES ('" +
            id + "'," +
            db.escape(commande.nom) + "," +
            db.escape(commande.montant) + "," +
            db.escape(commande.mail) +
            ",DATE(" + db.escape(date) +
            "),CURRENT_TIMESTAMP, '" + hash + "'," +
            client_id + ");";
    } else {
        return "INSERT INTO commande (id,nom,montant,mail,livraison,created_at, token) VALUES ('" +
            id + "'," +
            db.escape(commande.nom) + "," +
            db.escape(commande.montant) + "," +
            db.escape(commande.mail) +
            ",DATE(" + db.escape(date) +
            "),CURRENT_TIMESTAMP, '" + hash + "');";
    }
}

/**
 * Reformatage des Json de date en un objet pour BDD
 * @param jsonDate json contenant les dates
 * @returns {string} Date au format BDD
 */
function createDateObject(jsonDate) {
    let partieDate = jsonDate.date.split("-");
    let partieHeure = jsonDate.heure.split(":");
    return new Date(partieDate[2], partieDate[1], partieDate[0], partieHeure[0], partieHeure[1]).toISOString().slice(0, 19)
        .replace('T', ' ');
}


/**
 * Controle du token avec l'user
 * @param id identifiant de l'utilisateur
 * @param token token transmit par l'utilisateur
 * @returns {Promise<unknown>}
 */
function isValidTokenForSelectUser(id, token) {
    return new Promise((resolve, reject) => {
        token = token.split(" ")[1]; // recup du token
        if (isTokenUnaltered(token)) {
            // Recup Payload
            token = token.split(".")[1]; // recup subject du token
            // Decoder Payload
            let buffer = new Buffer(token, 'base64');
            token = buffer.toString('ascii');
            // Parse en JSON
            token = JSON.parse(token);

            // Recup client en BDD
            validAccessClientControlWithToken(id, token).then(result => {
                resolve(result);
            }).catch(err => {
                reject(err);
            });
        }
    });
}

/**
 * Verifier que le token ai le bon format
 * @param token token a tester
 * @returns {boolean} true si le token est ok
 */
function isTokenUnaltered(token) {
    let isGood = false;
    // Si token non vide
    if (typeof token !== "undefined") {
        let privateKey = fs.readFileSync("./private.key"); // recup fichier keypass pour verifier authenticite
        jwt.verify(token, privateKey, {algorithm: "HS256"}, (err, user) => {
            if (!err) {
                isGood = true;
            }
        });
    }
    return isGood;
}

/**
 * Fonction qui verifie si le token correspond bien a l'utilisateur demande
 * @param id identifiant de l'utilisateur a verifier
 * @param token token transmis par l'utilisateur
 * @returns {Promise<unknown>}
 */
function validAccessClientControlWithToken(id, token) {
    return new Promise(((resolve, reject) => {
        db.query("SELECT mail_client FROM client WHERE id = " + id + ";", function (error, result) {
            // Si problème de requete
            if (error) {
                reject(500);
            }
            // Si aucun resultat avec l'id saisi
            if (result.length === 0) {
                reject(404);
            } else {
                // Si mail provenant de BDD = au subject du token
                if (result[0].mail_client == token.sub) {
                    resolve(200);
                } else {
                    reject(403);
                }
            }
        });
    }));
}

/**
 * Recuperation des items venant d'une commande
 * @param id identifiant de la commande
 * @returns {Promise<unknown>}
 */
function getItemsFromCommande(id) {
    return new Promise((resolve, reject) => {
        let queryItem = "SELECT * FROM item where command_id='" + id + "'";
        db.query(queryItem, (err, result) => {
            if (err) {
                reject(500);
            }
            if (result.length == 0 || typeof result == "undefined") {
                reject(404);
            }
            let items = []
            for (const resultatSandwich of result) {
                items.push({
                    "uri": resultatSandwich.uri,
                    "libelle": resultatSandwich.libelle,
                    "tarif": resultatSandwich.tarif,
                    "quantite": resultatSandwich.quantite
                })
            }
            resolve(items);
        })
    })
}

/**
 * Recuperation du prix unitaire d'un item dans une commande
 * @param item object item à compter
 * @param id identifiant de la commande
 * @returns {Promise<unknown>}
 */
function getPrixUnitaire(item, id) {
    return new Promise((resolve, reject) => {
        axios.get("http://catalogue:8080" + item.uri).then((resultat) => {
            let montant = parseFloat(resultat.data.prix) * item.q;

            //Ajout dans la table Item
            let query = "INSERT INTO item (uri,libelle,tarif,quantite,command_id) VALUES(" + db.escape(item.uri) + "," + db.escape(resultat.data.nom) + "," + db.escape(resultat.data.prix)
                + "," + db.escape(item.q) + "," + db.escape(id) + ")";

            db.query(query, (err, result) => {
                if (err) {
                    res.status(500).send(err);
                }
            });
            resolve(montant);
        }).catch((erreur) => {
            reject(erreur);
        });
    });
}

/**
 * Recuperation du prix total
 * @param req Request PSR-7
 * @param id identifiant de la commande
 * @returns {Promise<unknown>}
 */
function getPrix(req, id) {
    return new Promise(function (resolve, reject) {
        let montant = 0;
        let promises = [];
        for (let item of req.body.commande.items) {
            promises.push(getPrixUnitaire(item, id));
        }
        Promise.all(promises).then(montants => {
            let total = 0;
            for (let montant of montants) {
                total += montant;
            }
            resolve(total);
        });

    });
}

/**
 * On vérifie la validité des formats des données postées
 * @param data données transmises
 * @returns {[boolean, string]}
 */
function verifFormat(data) {
    let dataIsOk = [true, ""];
    let date = createDateObject(data.commande.livraison);
    const regex = new RegExp("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$");
    if (!validator.isEmail(data.commande.mail)) {
        dataIsOk[0] = false;
        dataIsOk[1] += "Le format de l'adresse mail est incorrect - ";
    }
    if (!regex.test(data.commande.nom)) {
        dataIsOk[0] = false;
        dataIsOk[1] += "Le nom contient des caractères non-alphanumériques - ";
    }
    if (!validator.isAfter(date)) {
        dataIsOk[0] = false;
        dataIsOk[1] += "La date n'est pas au format adaptée";
    }
    return dataIsOk;
}

/**
 * Test si donnes obligatoires presentes
 * @param data les donnees a tester
 * @returns {[boolean, string]}
 */
function isMandatoryDataPresent(data) {
    let mandatoryData = [true, ""];
    if (data.commande.nom === undefined || data.commande.nom === "") {
        mandatoryData[0] = false;
        mandatoryData[1] = "Le nom doit être renseigné !"
    }
    if (data.commande.livraison === undefined || data.commande.nom === {}) {
        mandatoryData[0] = false;
        mandatoryData[1] = "La date et l'heure de livraison doivent être remplis !";
    } else {
        if (data.commande.livraison.date === undefined || data.commande.livraison.heure === undefined || data.commande.livraison.date === "" || data.commande.livraison.heure === "") {
            mandatoryData[0] = false;
            mandatoryData[1] = "La date et l'heure de livraison doivent être remplis, l'un des deux est vide !";
        }
    }
    if (data.commande.mail === null) {
        mandatoryData[0] = false;
        mandatoryData[1] = "L'adresse e-mail doit être renseignée !";
    }
    if (Array.isArray(data.commande.items)) {
        if (data.commande.items.length === 0) {
            mandatoryData[0] = false;
            mandatoryData[1] = "Le tableau items ne doit pas être vide !";
        }
    } else {
        mandatoryData[0] = false;
        mandatoryData[1] = "Le tableau 'items' doit être présent !";
    }
    return mandatoryData;
}

/**
 * fonction qui retourne un json avec le code d'erreur et le message
 * @param code
 * @return json
 */
function getMessageFromHTTPCode(code) {
    code = parseInt(code);
    let message = "";

    switch (code) {
        case 400:
            message = "Requête invalide : mauvaise requête ou paramètre manquant";
            break;
        case 401:
            message = "Accès non autorisé / Utilisateur non connecté";
            break;
        case 403:
            message = "Accès interdit, vous n'avez pas les droits suffisants";
            break;
        case 404:
            message = "Ressource non trouvée";
            break;
        case 405:
            message = "Méthode interdite";
            break;
        case 500:
            message = "Erreur du serveur";
            break;
    }

    message = JSON.stringify({code: code, message: message});

    return message;
}

/**
 * Verifier si email transmis egal a email provenant de BDD
 * @param postEmail email a verifier
 * @param id id de l'user
 * @returns {Promise<unknown>}
 */
async function checkEmails(postEmail, id) {
    return new Promise((resolve, reject) => {
        // Select email avec l'id
        let query = "SELECT mail_client FROM client WHERE id = " + id + ";";

        db.query(query, (err, result) => {
            if (err) {
                reject(500);
            } else if (result.length <= 0) {
                reject(404);
            } else {
                if (result[0].mail_client === postEmail) {
                    resolve(200);
                } else {
                    reject(401);
                }
            }
        });
    });
}