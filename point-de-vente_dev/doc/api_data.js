define({ "api": [
  {
    "type": "get",
    "url": "/",
    "title": "route d'accueil de l'api commande",
    "description": "<p>affiche un message de bienvenue</p>",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "string",
            "optional": false,
            "field": "messageBienvenue",
            "description": "<p>message de bienvenue</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "name": "Get"
  },
  {
    "type": "get",
    "url": "/commandes",
    "title": "affiche les commandes",
    "description": "<p>affiche les commandes</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "s",
            "description": "<p>statut des commandes à afficher</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "name": "GetCommandes"
  },
  {
    "type": "get",
    "url": "/commandes/:id",
    "title": "affiche les infos d'une commande",
    "description": "<p>affiche les infos d'une commande</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>, id de la commande concernée</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "json",
            "description": "<p>avec les infos de la commande</p>"
          }
        ]
      }
    },
    "version": "0.0.0",
    "filename": "./server.js",
    "group": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "groupTitle": "/home/thomas/Documents/ciasie/lebonsandwich/point-de-vente_dev/server.js",
    "name": "GetCommandesId"
  }
] });
