"use strict";

const express = require("express");
const mysql = require("mysql");

// Constantes
const PORT = 8080;
const HOST = "0.0.0.0";

// App
const bodyParser = require('body-parser');

const uuid = require('uuid/v1');

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

//--------------------------------------------------------------------
//                     routes
//--------------------------------------------------------------------

/**
 * @api {get} / route d'accueil de l'api commande
 * @apiDescription affiche un message de bienvenue
 * @apiSuccess {string} messageBienvenue message de bienvenue
 */
app.get("/", (req, res) => {
    res.satus(200).send("Bienvenue sur point de vente API\n");
});

/**
 * @api {get} /commandes affiche les commandes
 * @apiDescription affiche les commandes
 * @apiParam {Number} [s] statut des commandes à afficher
 */
app.get("/commandes", (req, res) => {

    let query;

    if(typeof req.query.s !== "undefined"){
        query = "select id, nom, created_at, livraison, status  from commande where status='" + req.query.s+ "' group by id, nom, created_at, livraison, status order by livraison,created_at asc";
    }
    else{
        query = "select id, nom, created_at, livraison, status  from commande group by id, nom, created_at, livraison, status order by livraison,created_at asc";
    }


    db.query(query, (error, resultat) => {
        if (error) {
            res.status(500);
            res.header("Content-Type", "application/json; charset=utf-8").end(getMessageFromHTTPCode(500));
        }

        let count = resultat.length;
        let page = 1;
        let size = 10;
        let nbPage = ((count / size));
        let offset = 1;

        // Passer ce booléen à false pour avoir le résultat de la question 1
        let pagination = true;
        if(typeof req.query.page !== "undefined"){
            page = req.query.page;
            if(page <= 0) page = 1;
            if(page > nbPage) page = nbPage;

            offset = page * size - size;
            pagination = true;

        }
        if(typeof req.query.size !== "undefined"){
            if(req.query.size > 0) size = req.query.size;

        }

        let retour;

        if(pagination){
            retour = {
                "type":"collection",
                "count": count,
                "links": {
                    "next": {
                        "href": "/commandes?page=" + ((parseInt(page)+1)) + "&size=" + size
                    },
                    "prev": {
                        "href": "/commandes?page=" + ((parseInt(page)-1)) + "&size=" + size
                    },
                    "first": {
                        "href": "/commandes?page=1&size=" + size
                    },
                    "last": {
                        "href": "/commandes?page=" + nbPage + "&size=" + size
                    },
                },
                "commands": [],
            };
            let range = parseInt(offset) + parseInt(size);
            for(let i = offset; i <= range - 1; i++){
                retour.commands.push({
                    "command" : resultat[i],
                    "links" : {
                        "self":  {"href" : "/commandes/" + resultat[i].id}
                    }
                })
            }
        }else{
            retour = {
                "type":"collection",
                "count": count,
                "commands": [],
            };
            resultat.forEach((element) => {

                retour.commands.push({
                    "command": element,
                    "links": {
                        "self":  {"href" : "/commandes/" + element.id}
                    }
                });
            })
        }
        res.setHeader('count', count);
        res.setHeader('size', size);
        res.status(200);
        res.end(JSON.stringify(retour));
    })
});

/**
 * @api {get} /commandes/:id affiche les infos d'une commande
 * @apiDescription affiche les infos d'une commande
 * @apiParam {String} id, id de la commande concernée
 * @apiSuccess {json} json avec les infos de la commande
 */
app.get("/commandes/:id", (req, res) => {
   let id = req.params.id;
   let query;
   let jsonResult = {
        "type": "resource",
        "links": {
            "self": "/commands/" + id + "/",
            "items": "/commands/" + id + "/items/"
        },
        "command": {
            "id": id,
            "created_at": "",
            "livraison": "",
            "nom": "",
            "mail": "",
            "montant": 0,
            "items": []
        }
    };
   query = "SELECT id,created_at,livraison,nom,mail,montant FROM commande WHERE id=?";

   db.query(query,[id],(err,result) => {
       jsonResult.command.created_at = result[0].created_at;
       jsonResult.command.livraison = result[0].livraison;
       jsonResult.command.nom = result[0].nom;
       jsonResult.command.mail = result[0].mail;
       jsonResult.command.montant = result[0].montant;

       query = "SELECT uri,libelle,tarif,quantite FROM item WHERE command_id=?";
       db.query(query, [id],(err, result) => {
           for (let sandwich of result) {
               jsonResult.command.items.push({"uri": sandwich.uri,"libelle": sandwich.libelle,"tarif": sandwich.tarif,"quantite": sandwich.quantite});
           }
           res.status(200).json(jsonResult);
       });
   });

});

//-------------------------------------------------------------------
//                     fin des routes
//-------------------------------------------------------------------

// ( à mettre en dernier après les routes)
/**
 * @root *
 * @method all
 * Bad request
 */
app.all("*", (req, res) => {
    res.setHeader('Content-Type', 'application/json');
    res.status(400);
    res.end(JSON.stringify({type: "error", error: 400, message: "Requête invalide : " + req.url}));
});

app.listen(PORT, HOST);
console.log(`Commande API Running on http://${HOST}:${PORT}`);

//créé la bdd
const db = mysql.createConnection({
    host: "mysql.commande",
    user: "command_lbs",
    password: "command_lbs",
    database: "command_lbs"
});

// connexion à la bdd
db.connect(err => {
    if (err) {
        throw err;
    }
    console.log("Connected to database");
});

//--------------------------------------------------------------------
//                  Fonctions
//--------------------------------------------------------------------

/**
 * fonction qui retourne un json avec le code d'erreur et le message
 * @param code
 * @return json
 */
function getMessageFromHTTPCode(code) {
    code = parseInt(code);
    let message = "";

    switch (code) {
        case 400:
            message = "Requête invalide : mauvaise requête ou paramètre manquant";
            break;
        case 401:
            message = "Accès non autorisé / Utilisateur non connecté";
            break;
        case 403:
            message = "Accès interdit, vous n'avez pas les droits suffisants";
            break;
        case 404:
            message = "Ressource non trouvée";
            break;
        case 405:
            message = "Méthode interdite";
            break;
        case 500:
            message = "Erreur du serveur";
            break;
    }

    message = JSON.stringify({code: code, message: message});

    return message;
}