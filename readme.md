# Le Bon Sandwich API
## réalisé par Maxence DUROY, Clément MERCIER, Théo LEGRAND et Thomas SERRES

### lien du dépôt git : <a href="https://gitlab.com/legrand82u/lebonsandwich">https://gitlab.com/legrand82u/lebonsandwich</a>

<p>P.S : Pour les routes nécessitant un json en entrée (ou en sortie), ces json sont conformes à ce qui est demandé dans les sujets de td.</p>

<p>P.P.S : Les docs pour chaque API est dans leur répertoire dans le répertoire doc.</p>


### Carnet de bord : 
<ul>
    <li>
        <b>TD 1 :</b> Tout le monde
    </li>
    <li>
        <b>TD 2 :</b> Clément, Maxence et Théo
    </li>
    <li>
        <b>TD 3 :</b>
        <ul>
            <li><b>Question 1 :</b> Clément et Maxence</li>
            <li><b>Question 2 :</b> Thomas</li>
            <li><b>Question 1 :</b> Théo puis Maxence l'a rejoint</li>
        </ul>
    </li>
    <li>
        <b>TD 4 :</b> Thomas puis rejoint par Théo
    </li>
    <li>
        <b>TD 5 :</b> Maxence et Théo
    </li> 
    <li>
        <b>TD 6 :</b>
        <ul>
            <li><b>Question 1 et 2 :</b> Clément et Thomas</li>
            <li><b>Question 3 et 4 :</b> Maxence et Théo</li>
        </ul>
    </li>
    <li>
        <b>TD 7 :</b> Thomas et Clément
    </li>    
    <li>
        <b>TD 8 :</b> Maxence et Théo
    </li>        
    <li><b>Code rewiews et refactoring : </b> Tout le monde</li>
    <li><b>Documentations : </b> Thomas et Clément</li>
</ul> 